这是一个WPF的自定义控件库，参考博主http://www.cnblogs.com/anding/p/4961215.html 的文章制作而成。

其中知识点请参考[/*梦里花落知多少*/](http://www.cnblogs.com/anding/p/4961215.html)的文章自行研究

![主要控件](https://git.oschina.net/uploads/images/2017/0905/215730_eba1dd2e_460418.png "2017-09-05_215714.png")

1.font-awesome字体图标
![字体图标](https://git.oschina.net/uploads/images/2017/0905/220420_f5f1ca65_460418.png "2017-09-05_220356.png")
可以到该地址下查看图标的十六进制值[font awesome](http://fontawesome.io/cheatsheet/)

2.图标按钮
![图标按钮](https://git.oschina.net/uploads/images/2017/0905/220721_41c07629_460418.png "2017-09-05_220723.png")

3文本框
![文本框](https://git.oschina.net/uploads/images/2017/0905/220837_e44eb409_460418.png "2017-09-05_220842.png")

4.checkbox、radiobutton等
![按钮组](https://git.oschina.net/uploads/images/2017/0905/220943_41ac245a_460418.png "2017-09-05_220939.png")

5.日期时间控件
![日期时间](https://git.oschina.net/uploads/images/2017/0905/221053_9c4edcbd_460418.png "2017-09-05_221108.png")
![日期选择](https://git.oschina.net/uploads/images/2017/0905/221200_3dee3e7a_460418.png "2017-09-05_221215.png")

6.其他
![其他控件](https://git.oschina.net/uploads/images/2017/0905/221341_f3348570_460418.png "2017-09-05_221301.png")
![treeview](https://git.oschina.net/uploads/images/2017/0905/221358_3246d3a7_460418.png "2017-09-05_221337.png")
![进度条](https://git.oschina.net/uploads/images/2017/0905/221411_3a1fa3f2_460418.png "2017-09-05_221350.png")